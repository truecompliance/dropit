<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use File;
use Auth;
use Storage;

class CertificateController extends Controller
{
    public function store(Request $request)
    {
        // validate form
        $this->validate($request, [
            'certificate' => 'required|mimes:pdf,zip'
        ]);

        $user = Auth::user();
        $path = str_replace(' ', '-', strtolower($user->name) );
        $date = date('Y-m-d');
        $filename = $request->file('certificate')->getClientOriginalName();

        //Storage::disk('s3')->put($path.'/'.$date.'/', $request->file('certificate'));
        $request->file('certificate')->storeAs($path.'/'.$date.'/', $filename, 's3');

        if($request->ajax()){
            return true;
        }else{
            return view('home');
        }
    }

    /**
     * Ajax endpoint for the drag and drop file store
     *
     * This runs the above store method for each file dragged into the frontend box, and returns a success or fail boolean to the frontend.
     *
     * @param Request $request
     *
     * @return string
     */
    public function storeDrag(Request $request)
    {
        $loaded = $this->store($request);

        if($loaded){
            return 'true';
        }else{
            return 'false';
        }
    }
}
