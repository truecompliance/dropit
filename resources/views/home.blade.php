@extends('layouts.app')

@section('css')
<style>
    #filedrag
    {
        display: none;
        font-weight: bold;
        text-align: center;
        padding: 1em 0;
        margin: 1em 0;
        color: #555;
        border: 2px dashed #555;
        border-radius: 7px;
        cursor: default;
    }

    #filedrag.hover
    {
        color: #f00;
        border-color: #f00;
        border-style: solid;
        box-shadow: inset 0 3px 4px #888;
    }

    #filedrag h3{
        font-size: 50px;
        margin: 100px 0;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    @include('partials.js-ajax-manager')

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    // getElementById
    function $id(id) {
        return document.getElementById(id);
    }

    //
    // output information
    function Output(msg) {
        var m = $id("messages");
        m.innerHTML = m.innerHTML + msg;
    }

    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }

    //
    // initialize
    function Init() {

        var fileselect = $id("fileselect"),
            filedrag = $id("filedrag"),
            submitbutton = $id("submitbutton");

        // file select
        fileselect.addEventListener("change", FileSelectHandler, false);

        // is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {

            // file drop
            filedrag.addEventListener("dragover", FileDragHover, false);
            filedrag.addEventListener("dragleave", FileDragHover, false);
            filedrag.addEventListener("drop", FileSelectHandler, false);
            filedrag.style.display = "block";

            // remove submit button
            submitbutton.style.display = "none";
        }

    }

    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.className = (e.type == "dragover" ? "hover" : "");
    }

    // file selection
    function FileSelectHandler(e) {

        // cancel event and hover styling
        FileDragHover(e);

        // fetch FileList object
        var files = e.target.files || e.dataTransfer.files;
        //console.log(files.length);
        $('#upload').hide();
        Output(
            '<p><strong><span class="number-loaded">0</span> / ' + files.length +
            "</strong> files uploaded. Please don't leave this page until upload complete."
        );
        Output(
            '<div class="progress">' +
              '<div class="progress-bar" data-counter="0" data-increment="' + files.length + '" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">' +
                '0%' +
              '</div>' +
            '</div>'
            //'<div class="progress-bar" data-increment="' + files.length + '"><span class="progress" style="width:0;"></span></div>'
        );

        // process all File objects
        for (var i = 0, f; f = files[i]; i++) {
            ParseFile(f);
        }

        ajaxManager.run();
    }

    function ParseFile(file) {

        var data = new FormData();
        var name = file.name;
        var safe = name.replace(/\s+/g, '-');
        file.name = safe;

        data.append('certificate', file, safe);

        // Send file via ajax to php endpoint to add to job queue
        ajaxManager.addReq({
            type: "POST",
            url: "/certificate/drag",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data, status){
                //console.log(status);
                if(data == 'true'){
                    // Output(
                    //     "<p><strong>" + file.name +
                    //     "</strong> added to the process queue"
                    // );
                } else {
                    Output(
                        "<p><strong>" + file.name +
                        "</strong> <strong class=\"text-danger\">failed</strong>, please try again"
                    );
                }

                // Increment the progress bar
                incrementProgressBar();
            },
            error: function(data) {
                console.log(data);
                Output(
                    //data
                    "<p><strong>" + file.name +
                    "</strong> <strong class=\"text-danger\">failed</strong>, please try again"
                );

                // Increment the progress bar
                incrementProgressBar();
            }
        });
    }

    function incrementProgressBar(){
        // Increment the progress bar
        var inc = $('.progress-bar').data('increment');

        counter = $('.progress-bar').attr('data-counter');

        var incPercentage = 100 / inc;
        var newWidth = incPercentage * (parseInt(counter) + 1);
        $('.progress-bar').css('width', newWidth+'%');
        $('.progress-bar').attr('aria-valuenow', newWidth);
        $('.progress-bar').html(newWidth.toFixed(0)+'%');
        $('.progress-bar').attr('data-counter', parseInt(counter) + 1);

        $('span.number-loaded').html( parseInt(counter) + 1 );

        if( parseInt(counter) + 1 == inc){
            // We are finished!
            Output(
                "<p><strong>Upload completed!</strong> Thanks for using True Compliance Drop It.</p><p><strong><a href=\"/home\">Load more files!</a></strong></p>"
            );
        }
    }
</script>
@endsection

@section('content')
<div class="container js">
    <div class="row">
        <!-- <div class="col-md-10 col-md-offset-1"> -->
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Load New Certificates</strong></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::open( [ 'route' => [ 'certificate.store' ], 'method' => 'post', 'files' => true, 'id' => 'upload'] ) !!}

                            <fieldset>

                            <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />

                            <div>

                                <div id="filedrag"><h3>Drag PDF’s into this box to upload</h3></div>
                            </div>

                            <p>The maximum size of each individual file is 20mb, and the most number of files you can drop at once is 200.</p>

                            <p>Or... click the button below to browse for files:</p>
                            <input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
                            <br />
                            <div id="submitbutton">
                                <button>Upload Files</button>
                            </div>

                            </fieldset>

                            </form>

                            <div id="messages">
                                <!-- <p>Status Messages</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
